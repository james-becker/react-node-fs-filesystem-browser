const { ApolloServer } = require('apollo-server-express')
const { ApolloServerPluginDrainHttpServer } = require('apollo-server-core')
const express = require('express')
const http = require('http')

const { typeDefs, resolvers } = require('./schema.js')

const path = require('path')

async function startApolloServer(typeDefs, resolvers) {
  // Required logic for integrating with Express
  const app = express()
  const httpServer = http.createServer(app)

  // Have Node serve the files for our built React app
  app.use('/', express.static(path.join(__dirname, 'client/build')))

  // Handle GET requests to /api route
  app.get('/api', (req, res) => {
    res.json({ message: "Hello from server!" })
  });

  // All other GET requests not handled before will return our React app
  app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'))
  });

  // Same ApolloServer initialization as before, plus the drain plugin.
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
  });

  // More required logic for integrating with Express
  await server.start();
  server.applyMiddleware({
    app,

    // By default, apollo-server hosts its GraphQL endpoint at the
    // server root. However, *other* Apollo Server packages host it at
    // /graphql. Optionally provide this to match apollo-server.
    path: '/graphql'
  });

  // Modified server startup
  const port = process.env.PORT || 5000
  await new Promise(resolve => httpServer.listen({
    hostname: '0.0.0.0',
    port: port,
  }, resolve));
  console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`)
}

startApolloServer(typeDefs, resolvers)