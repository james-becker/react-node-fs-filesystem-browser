const { gql } = require('apollo-server-express');
const _ = require('lodash')

const path = require('path')
const fs = require('fs')

const getDirectory = (dir) => {
  elements = fs.readdirSync(dir)
  const contents = elements.map(elementName => {
    const name =              elementName.match(/(\.{0,1}[^.]+)/)[0]
    const absPath =           `${dir}/${elementName}`
    const noRead =            readStatus(absPath)
    const ext =               path.extname(elementName)
    const stats =             fs.statSync(absPath)
    const mode =              stats.mode
    const user =              stats.uid
    const group =             stats.gid
    const size =              stats.size
    const lastRead =          stats.atime
    const lastWrite =         stats.mtime
    const isDirectory =       stats.isDirectory()
    const isFile =            stats.isFile()
    const isBlockDevice =     stats.isBlockDevice()
    const isCharacterDevice = stats.isCharacterDevice()
    const isSymbolicLink =    stats.isSymbolicLink()
    const isFIFO =            stats.isFIFO()
    const isSocket =          stats.isSocket()

    const typeClasses = Object.keys(_.pickBy({
      isDirectory,
      isFile,
      isBlockDevice,
      isCharacterDevice,
      isSymbolicLink,
      isFIFO,
      isSocket,
      noRead
    }, function(v, _k) { return v }))

    return {
      name,
      absPath,
      ext,
      mode,
      user,
      group,
      size,
      lastRead,
      lastWrite,
      typeClasses
    }
  })
  return {
    dir,
    contents
  }
}

function readStatus(filename) {
  try{
    fs.accessSync(filename, fs.constants.R_OK)
    return false;
  } catch(e) {
    return true;
  }
}


const typeDefs = gql`
  type DirectoryElement {
    name: String
    absPath: String
    ext: String
    mode: Int
    user: Int
    group: Int
    size: Int
    lastRead: String
    lastWrite: String
    typeClasses: [String]
  }

  type Directory {
    dir: String
    contents: [DirectoryElement]
  }

  type Query {
    getDir(dir: String): Directory
    getHello: String
  }
`;

const resolvers = {
  Query: {
    getDir: (parent, { dir }, context,info) => {
      return getDirectory(dir)
    }
  }
  // Directory: {
  //   dir: (parent,args,context,info) => {
  //     console.log('directory parent:')
  //     console.log(parent)
  //     return parent.directory
  //   },
  //   directoryElement: (parent,args,context,info) => {
  //     console.log('directoryElement parent:')
  //     console.log(parent)
  //     return parent.directory
  //   }
  // }
};

module.exports = { typeDefs, resolvers }
