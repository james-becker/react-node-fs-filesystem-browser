import React from 'react'
import './AttrColumn.scss'
import { format } from 'date-fns'
import { setHovered } from '../../store'
import { useSelector, useDispatch } from 'react-redux'

const ICON_MAPPING = {
  isFile: 'fa fa-file',
  isDirectory: 'far fa-folder',
}

const ATTR_LABELS = {
  icon:         'Icon',
  name:         'Name',
  absPath:      'Path',
  ext:          'Ext.',
  mode:         'Mode',
  user:         'User',
  group:        'Group',
  size:         'Size',
  lastRead:     'Read Date',
  lastWrite:    'Write Date',
}

function formatData(value, attr) {
  return ['lastRead', 'lastWrite'].includes(attr) ? 
    format(parseInt(value), 'LLL. d, y h:mm bbbb')
    : value
}

function AttrColumn(props) {
  const state = useSelector(state => state.directory)
  const uiState = useSelector(state => state.ui)
  const dispatch = useDispatch()

  const attr = props.attr

  const handleOnMouseEnter = function({ attr, index }) {
    dispatch(setHovered({ attr, index }))
  }

  return (
    <div
      className={`attr-column`}
      style={{ 
        borderLeft: props.dragOver && 'solid var(--analysis) 10px',
        zIndex: props.dragOver && 40000,
        opacity: props.dragOver && .5,
        cursor: 'pointer',
      }}
    >
      <div style={{
        position: 'relative',
        height: 'var(--header-height)'
      }}></div>
      <div
        className='col'
        key={attr}
        id={attr}
        draggable
        onDragStart={props.handleDragStart}
        onDragOver={props.handleDragOver}
        onDrop={props.handleOnDrop}
        onDragEnter={props.handleDragEnter}
        style={{
          position: 'fixed',
        }}
      >
        { ATTR_LABELS[attr] }
      </div>

      {props.attrValues && props.attrValues.map((value, index) => {
        return (
          <div
            className={`data ${attr} data-${index}`}
            key={index}
            onClick={() => props.targetData({ attr, index })}
            value={{ attr, index }}
            onMouseEnter={() => { handleOnMouseEnter({ attr, index }) }}
            style={{
              background: uiState.hovered?.index === index && 'var(--action)',
            }}
          >
            {attr !== 'icon' && formatData(value, attr)}
            {attr === 'icon' && (
              <i className={`${ICON_MAPPING[Object.values(state.contents[index].typeClasses)[0]]}`}></i>
            )}
          </div>
        )
      })}
    </div>
  )
}

export default AttrColumn
