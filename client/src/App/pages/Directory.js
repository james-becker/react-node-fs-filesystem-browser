import React from 'react'
import AttrColumn from '../AttrColumn/AttrColumn'
import './Directory.scss'
import ApolloClient from 'apollo-boost'
import { gql } from 'apollo-boost'
import { ApolloProvider } from '@apollo/react-hooks'
import { useSelector, useDispatch } from 'react-redux'
import { setDirectory, setAttrs, setContents, setDragOver } from '../../store'

const PORT = process.env.PORT || 5000

const client = new ApolloClient({
  uri: '/graphql'
})

const GET_DIRECTORY = gql`
    query getDir($dir: String) {
      getDir(dir: $dir) {
        dir,
        contents {
          name,
          absPath,
          ext,
          mode,
          user,
          group,
          size,
          lastRead,
          lastWrite,
          typeClasses
        }
      }
    }
  `

function Directory(props) {
  const dirState = useSelector(state => state.directory)
  const { dir, contents, attrs } = dirState

  const dispatch = useDispatch()

  const fetchDirectory = async(dir) => {
    const res = await client.query({
      query: GET_DIRECTORY,
      variables: {
        dir: dir
      }
    })
    dispatch(setDirectory(res.data.getDir.dir))
    dispatch(setContents(res.data.getDir.contents))
  }

  fetchDirectory(dir)

  const changeDirectory = (elementIndex) => {
    const element = dirState.contents[elementIndex]
    if (element.typeClasses.includes('isDirectory')) {
      fetchDirectory(element.absPath)
    }
  }

  const parentDir = (dir) => {
    return dir.split('/').slice(0,-1).join('/') || '/'
  }

  const applyFilters = (contents) => {
    if (dirState.preferences.hideDot) {
      contents.filter(element => element.name[0] !== '.')
    }
  }

  const targetElement = ({ attr, index }) => {
    changeDirectory(index)
  }

  const handleDragStart = e => {
    const { id } = e.target
    const idx = dirState.attrs.indexOf(id)
    e.dataTransfer.setData('colIdx', idx)
  };

  const handleDragOver = e => e.preventDefault()
  
  const handleDragEnter = e => {
    const attr = e.target.id;
    dispatch(setDragOver(attr))
  };

  const handleOnDrop = e => {
    const { id } = e.target
    const droppedColIdx = dirState.attrs.indexOf(id)
    const draggedColIdx = e.dataTransfer.getData("colIdx")
    const tempCols = [...dirState.attrs]
    tempCols[draggedColIdx] = dirState.attrs[droppedColIdx]
    tempCols[droppedColIdx] = dirState.attrs[draggedColIdx]
    dispatch(setAttrs(tempCols))
    dispatch(setDragOver(''))
  }

  applyFilters(contents)

  return (
    <ApolloProvider client={client}>
      <div className='directory'>

        <div className='dir-heading'>
          <div className='dir-nav'>
            <div onClick={() => { fetchDirectory(parentDir(dir)) }}>
              <i className={'fa fa-level-up-alt fa-lg'}></i>
            </div>
            {/* TODO */}
            {/* <div onClick={() => { fetchDirectory(parentDir(dir)) }}>
              <i className={'fa fa-caret-right fa-lg'}></i>
            </div> */}
          </div>

          <h1 className='dir-name'>
            {dir.replace('//','/')}
          </h1>
        </div>

        <div className='dir-contents'>
          {attrs.length && attrs.map((attr, index) => {
            return(
              <AttrColumn
                attr={attr}
                attrValues={contents ? contents.map(el => el[attr]) : ''}
                key={index}
                id={index}
                targetData={targetElement}
                dragOver={attr === dirState.dragOver}
                handleDragStart={handleDragStart}
                handleDragOver={handleDragOver}
                handleOnDrop={handleOnDrop}
                handleDragEnter={handleDragEnter}
              />
            )})}
        </div>
      </div>
    </ApolloProvider>
  )
}

export default Directory;