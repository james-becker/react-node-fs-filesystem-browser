import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.scss'
import PreferencePanel from './PreferencePanel/PreferencePanel'
import Directory from './pages/Directory'
import { Layout, Dropdown, Menu, Button } from 'antd'
import { DownOutlined } from '@ant-design/icons'
import chroma from 'chroma-js'
import { createGlobalStyle } from 'styled-components'
import { useSelector } from 'react-redux'

const { Header, Content, Sider, Footer } = Layout

function App() {
  const theme = useSelector(state => state.theme.theme)
  const dirState = useSelector(state => state.directory)
  const contentsCount = dirState.contents.length
  const themeDefinition = Object.values(theme)
  const themePreview = themeDefinition.slice(2)
  
  const GlobalStyle = createGlobalStyle`
    * {
      --background:               ${theme.background};
      --background--muted:        ${chroma(theme.background).alpha(.96)};
      --text--primary:            ${theme.text};
      --text--primary--alpha:     ${chroma(theme.text).alpha(0.4)};
      --border:                   ${chroma(theme.text).darken(2).desaturate(2).hex()};
      --text--secondary:          royalblue;
      --action:                   ${chroma.mix(theme.background, theme.action, .42).hex()};
      --action--secondary:        ${chroma.mix(theme.background, theme.action, .22).hex()};
      --alert:                    ${theme.alert};
      --analysis:                 ${theme.analysis};
      --up:                       ${theme.up};
      --down:                     ${theme.down};
      --accent:                   ${chroma(theme.action).alpha(1)};
      --row-height:               ${theme.rowHeight}px;
      --header-height:            ${theme.rowHeight*1.2}px;
      --column-width:             200px;
      --font-size                 ${theme.fontSize}em;
      --font-size--heading        ${theme.fontSizeHeading}rem;
      --font--data:               ${theme.fontPrimary}, source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace;
      --font--data:               ${theme.fontData}, source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace;
      border-radius:              0 !important; // I know this is bad to have at such a high level, but I'm slapping this together.
      border-width:               1px !important;
    }
  `
  
  // TODO put inside PreferencePanel, probably
  const menu = (
    <Menu
      style={{
        height: '315px',
        width: '245px',
        borderRadius: '0px',
        border: 'var(--border) 1px solid',
        background: theme.background,
        padding: '10px',
      }}>
      <PreferencePanel />
    </Menu>
  )

  return (
    <BrowserRouter>
        <GlobalStyle />
        <div className='app'>
          <Layout style={{ height: '100vh' }}>
            <Header style={{
              height: '40px',
              borderBottomColor: 'var(--border)',
              borderBottomWidth: '1px',
              borderBottomStyle: 'solid',
              background: 'var(--action--secondary)',
            }}>
              <div
                style={{
                  float: 'left',
                  color: 'var(--alert)',
                  fontSize: '2rem',
                  fontStyle: 'italic',
                  fontWeight: 100,
                  height: '100%',
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                SabiFile <span style={{ fontSize: '.5em'}}>v0.1</span>
              </div>
              <Menu theme='dark' mode='horizontal' style={{
                background: 'var(--action--secondary)',
                opacity: 1,
                display: 'flex',
                justifyContent: 'flex-end',
                height: '100%',
              }}>
                <Menu.Item 
                  className='sabi-menu'
                  style={{
                    background: 'transparent',
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'middle',
                  }}
                >
                  <Dropdown overlay={menu} trigger={['click']}>
                    <Button
                      style={{
                        border: 'none',
                        background: 'transparent',
                        height: '100%',
                      }}
                    >
                      <a className='ant-dropdown-link' style={{display:'flex','flexDirection':'row'}} onClick={e => e.preventDefault()}>
                        <div style={{
                          display: 'grid',
                          gap: '1px',
                          gridTemplateColumns: '4px 4px 4px',
                          gridTemplateRows: '4px 4px',
                          paddingRight: '10px'
                        }}>
                          {themePreview.map((color, i) =>
                            <div key={i} style={{background: color}}></div>
                          )}
                        </div>
                        <DownOutlined style={{ color: 'var(--text--primary)' }}/>
                      </a>
                    </Button>
                  </Dropdown>
                </Menu.Item>
              </Menu>
            </Header>
            <Layout style={{
              background: 'var(--background)',
            }}>
              <Content style={{
                background: 'var(--background)',
                overflow: 'auto',
                borderBottomColor: 'var(--border)',
                marginTop: '70px',
              }}>
                <Routes>
                  <Route exact path='/' element={<Directory theme={theme} />} />
                </Routes>
              </Content>
              {/* <Sider style={{
                background: 'var(--background)'
              }} /> */}
            </Layout>
            <Footer style={{
              background: 'var(--background)',
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'flex-end',
              color: 'var(--text--primary)',
            }}>
              { contentsCount === 1 ? '1 item' : `${dirState.contents.length} items` }
            </Footer>
          </Layout>
        </div>
    </BrowserRouter>
  )
}

export default App;