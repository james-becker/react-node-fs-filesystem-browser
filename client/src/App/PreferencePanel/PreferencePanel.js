import './PreferencePanel.scss'
import { Select } from 'antd'
import { useSelector, useDispatch } from 'react-redux'
import { loadTheme } from '../../store'

function PreferencePane(props) {
  const { Option } = Select;

  const theme = useSelector(state => state.theme.theme)
  const themes = useSelector(state => state.theme.themes)
  const { name, rowHeight, fontSize, fontSizeHeading, font, fontData, ...themePreview } = theme

  const dispatch = useDispatch()

  return (
    <div className={`preference-panel`}>
      <h3 style={{
        color: 'var(--text--primary)',
        fontSize: '.9rem',
        fontWeight: 700
      }}>Theme</h3>
      <Select
        defaultValue={theme.name}
        labelInValue
        onSelect={(value, option) => dispatch(loadTheme(value, option))}
        style={{ width: '148px' }}
      >
        {themes.map((t, i) => {
          return (
            <Option
              key={i}
              value={t.value}
            >
              {t.name}
            </Option>
          )
        })}
      </Select>

      <div className='prefs-colors' style={{
        margin: '10px 0',
      }}>
        {Object.entries(themePreview).map(([k, v], i) => (
          <div
            key={{ i }}
            style={{
              display: 'flex',
              flexDirection: 'row',
              color: 'var(--text--primary)',
            }}
          >
            <div
              style={{
                height: '21px',
                width: '21px',
                background: v,
                border: '1px solid var(--border)',
                marginRight: '4px',
              }}
            />
            <div>{k}</div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default PreferencePane