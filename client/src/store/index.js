import { createSlice, configureStore } from '@reduxjs/toolkit'
import chroma from 'chroma-js'
import THEMES from './themes.js'

const themeState = {
  theme: {
    name:       'Halcyon',
    background: chroma(29,36,51).hex(),
    text:       '#fed177',
    action:     chroma(47,59,84).luminance(.1).hex(),
    alert:      '#ff9e14',
    analysis:   '#c099ff',
    up:         '#b0ea76',
    down:       '#fb5369',
    rowHeight:  42,
    fontSize:   1,
    fontSizeHeading: 1.2,
    font:       'Iosevka Extra',
    fontData:   'Iosevka Extra',
  },
  themes: THEMES.map(theme => {
    return { value: theme.name }
  }),
}

const directoryState = {
  error: false,
  dir: '/',
  contents: [],
  dragOver: '',
  attrs: [
    'icon',
    'name',
    'ext',
    'size',
    'mode',
    'user',
    'group',
    'lastRead',
    'lastWrite'
  ],
  preferences: {
    hideDot: false,
  }
}

const uiState = {
  hovered: {
    index: null,
  }
}

export const themeSlice = createSlice({
  name: 'theme',
  initialState: themeState,
  reducers: {
    loadTheme: (state, action) => {
        state.theme = THEMES.find(theme => theme.name === action.payload.value)
    },
  }
})

export const directorySlice = createSlice({
  name: 'directory',
  initialState: directoryState,
  reducers: {
    setDirectory: (state, action) => {
      state.dir = action.payload
    },
    setAttrs: (state, action) => {
      state.attrs = action.payload
    },
    setContents: (state, action) => {
      state.contents = action.payload
    },
    setDragOver: (state, action) => {
      state.dragOver = action.payload
    }
  }
})

export const uiSlice = createSlice({
  name: 'ui',
  initialState: uiState,
  reducers: {
    setHovered: (state, action) => {
      state.hovered = {
        index: action.payload.index,
        attr: action.payload.attr,
      }
    },
  }
})

export const { loadTheme }                                        = themeSlice.actions
export const { setDirectory, setAttrs, setContents, setDragOver } = directorySlice.actions
export const { setHovered }                                       = uiSlice.actions

export const store = configureStore({
    reducer: {
        theme:      themeSlice.reducer,
        directory:  directorySlice.reducer,
        ui:         uiSlice.reducer,
    }
})