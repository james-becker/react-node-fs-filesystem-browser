import React from 'react'
import { render } from 'react-dom'
import App from './App/App'
import { store } from './store'
import { Provider } from 'react-redux'
import 'antd/dist/antd.css'

render((
    <Provider store={store}>
        <App/>
    </Provider>
), document.getElementById('root'));
